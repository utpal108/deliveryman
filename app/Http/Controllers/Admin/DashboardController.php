<?php

namespace App\Http\Controllers\Admin;

use App\CoverageArea;
use App\CustomerOrder;
use App\Http\Controllers\Controller;
use App\TeamMember;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $customers=User::where([['user_type','customer'],['status',1]])->get()->count();
        $drivers=User::where([['user_type','driver'],['status',1]])->get()->count();
        $team_members=TeamMember::all()->count();
        $coverage_areas=CoverageArea::all()->count();
        $delivery_requests=CustomerOrder::with('customer')->where('status','ordered')->get()->count();
        $delivered_products=CustomerOrder::where('status','delivered')->get()->count();
        return view('admin.dashboard.index',compact('customers','drivers','team_members','coverage_areas','delivery_requests','delivered_products'));
    }
}
