<section class="ic-bottom-section">
    <div class="container">
        <div class="inner">
            <div class="image">
                <img src="/storage/{{ $drive_with_us->contents['drive_with_us_image'] ?? '' }}" alt="drive us image" class="wow fadeInLeft" data-wow-delay=".5s">
            </div>
            <div class="ic-content wow fadeInRight" data-wow-delay=".5s">
                <div class="text">
                    {{ $drive_with_us->contents['title'] ?? '' }}
                </div>
                <h1> {{ $drive_with_us->contents['subtitle'] ?? '' }}</h1>
                <a href=" {{ $drive_with_us->contents['url'] ?? '' }}" class="ic-btn-primary">Drive with us</a>
            </div>
        </div>
    </div>
</section>
