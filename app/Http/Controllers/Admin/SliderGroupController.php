<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SliderGroup;
use Illuminate\Http\Request;

class SliderGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider_groups=SliderGroup::all();
        return view('admin.slider.slider_group.index',compact('slider_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.slider_group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        SliderGroup::create($request->all());
        flash('Slider group created successfully')->success();
        return redirect()->action('Admin\SliderGroupController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider_group=SliderGroup::find($id);
        return view('admin.slider.slider_group.edit',compact('slider_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider_group=SliderGroup::find($id);
        $slider_group->group_name=$request->group_name;
        $slider_group->save();
        flash('Slider group updated successfully')->success();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SliderGroup::destroy($id);
        flash('Slider group deleted successfully')->success();
        return redirect()->action('Admin\SliderGroupController@index');
    }
}
