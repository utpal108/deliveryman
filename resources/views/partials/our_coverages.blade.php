<div class="row">
    @foreach($coverage_areas as $coverage_area)
        <div class="col-md-4 col-lg-4">
            <div class="single-content wow fadeIn">
                <div class="image">
                    <img src="/storage/{{ $coverage_area->area_image }}" alt="">
                </div>
                <div class="text"><h3>{{ $coverage_area->area_name }}</h3></div>
            </div>
        </div>
    @endforeach
</div>
