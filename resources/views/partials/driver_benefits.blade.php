<div class="row">
    @foreach($driver_benefits as $driver_benefit)
        <div class="col-sm-6 col-md-6 col-lg-4">
            <div class="ic-single-feature">
                <div class="ic-icon">
                    <i class="fa {{ $driver_benefit->icon }}" aria-hidden="true"></i>
                </div>
                <h2>{{ $driver_benefit->title }}</h2>
                <p>{{ $driver_benefit->details }}</p>
            </div>
        </div>
    @endforeach
</div>
