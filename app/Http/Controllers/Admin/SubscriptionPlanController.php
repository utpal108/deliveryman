<?php

namespace App\Http\Controllers\Admin;

use App\SubscriptionPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscription_plans=SubscriptionPlan::where('status',1)->get();
        return view('admin.subscription.index',compact('subscription_plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subscription.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'icon'=>'required',
            'title'=>'required',
            'duration_days'=>'required|numeric',
            'regular_price'=>'required|numeric',
            'discount_price'=>'required|numeric',
            'status'=>'required|numeric|min:0|max:1',
            'description'=>'required',
        ]);
        SubscriptionPlan::create($request->all());
        flash('New subscription plan created successfully');
        return redirect()->action('Admin\SubscriptionPlanController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription_plan=SubscriptionPlan::findOrFail($id);
        return view('admin.subscription.edit',compact('subscription_plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'icon'=>'required',
            'title'=>'required',
            'duration_days'=>'required|numeric',
            'regular_price'=>'required|numeric',
            'discount_price'=>'required|numeric',
            'status'=>'required|numeric|min:0|max:1',
            'description'=>'required',
        ]);

        $subscription_plan=SubscriptionPlan::findOrFail($id);
        $subscription_plan->icon=$request->icon;
        $subscription_plan->title=$request->title;
        $subscription_plan->duration_days=$request->duration_days;
        $subscription_plan->regular_price=$request->regular_price;
        $subscription_plan->discount_price=$request->discount_price;
        $subscription_plan->status=$request->status;
        $subscription_plan->description=$request->description;
        $subscription_plan->save();
        flash('Subscription plan updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubscriptionPlan::destroy($id);
        flash('Subscription Plan Deleted Successfully');
        return redirect()->action('Admin\SubscriptionPlanController@index');
    }
}
