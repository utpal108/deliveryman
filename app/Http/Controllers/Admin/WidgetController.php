<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Forum;
use App\Widget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $widgets=Widget::all();
        return view('admin.widget.index',compact('widgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(403,'Unauthorized');
        return view('admin.widget.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        Widget::create($request->all());
        flash('New Widget created successfully');
        return redirect()->action('Admin\WidgetController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $widget=Widget::findOrFail($id);
        return view('admin.widget.'.$widget->template, compact('widget'));
    }

    public function update(Request $request, $id)
    {
        $widget=Widget::findOrFail($id);
        $allData=$request->contents;
        if ($request->hasFile('drive_with_us_image')){
            $path=$request->file('drive_with_us_image')->store('images');
            $image = Image::make(Storage::get($path))->resize(352, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode();
            Storage::put($path, $image);
            $allData['drive_with_us_image']=$path;
            if (isset($widget->contents['drive_with_us_image'])){
                Storage::delete($widget->contents['drive_with_us_image']);
            }
        }
        elseif(isset($widget->contents['drive_with_us_image'])){
            $allData['drive_with_us_image']=$widget->contents['drive_with_us_image'];
        }
        $widget->contents=$allData;
        $widget->save();
        flash('Widget updated successfully');
        return redirect()->back();
    }

}
