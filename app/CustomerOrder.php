<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
    public function customer(){
        return $this->belongsTo('App\User','customer_id');
    }

    public function product_received(){
        return $this->belongsTo('App\User','received_by');
    }

    public function driver(){
        return $this->belongsTo('App\User','driver_id');
    }

    public function driver_assigned(){
        return $this->belongsTo('App\User','driver_assigned_by');
    }
}
