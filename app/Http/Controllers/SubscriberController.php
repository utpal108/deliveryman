<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;

class SubscriberController extends Controller
{
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'email' => 'required|unique:subscribers',
        ]);

        if ($validation->fails()){
            session()->flash('response_message', ['status'=>'success','title'=>'Subscribed successfully', 'message'=>'You are already subscribed']);
        }
        else{
            Subscriber::create($request->all());
            session()->flash('response_message', ['status'=>'success','title'=>'Subscribed successfully', 'message'=>'You will get regular update']);
        }
        return redirect()->back();
    }
}
