@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <!-- breadcrumb -->
    <div class="ic-breadcrumb">
        <div class="container">
            <div class="inner">
                <h2>{{ ucwords($page->page_title) }}</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">{{ ucwords($page->page_title) }}</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <div class="about ic-other-section">
        <!-- top content start -->
        <section class="ic-top-section">
            <div class="container">
                <h2 class="primary-color">{{ $page->contents['about_us_title'] ?? '' }}</h2>
                <h1>{{ $page->contents['about_us_subtitle'] ?? '' }}</h1>
                {!! $page->contents['about_us_details'] ?? '' !!}
            </div>
        </section>
        <!-- top content end -->
        <!-- mid content -->
        <section class="ic-top-section ic-mid-section">
            <div class="container">
                <h2 class="primary-color">{{ $page->contents['team_member_title'] ?? '' }}</h2>
                <h1>{{ $page->contents['team_member_subtitle'] ?? '' }}</h1>
                {!! $page->contents['team_member_details'] ?? '' !!}
                @include('partials.team_members')
            </div>
        </section>
        <!-- mid content end -->
        <!-- last content -->
        @include('partials.take_service')
        <!-- last content end -->
    </div>
@endsection

@section('script')

@endsection
