
<div class="widget-body">
    <legend>
        Form data for subscription plan
    </legend>

    <div class="form-group">
        <label>Subscription plan title</label>
        <input type="text" class="form-control" name="contents[subscription_plan_title]" value="{{ $page->contents['subscription_plan_title'] ?? '' }}" required/>
    </div>
    <div class="form-group">
        <label>Subscription plan subtitle</label>
        <input type="text" class="form-control" name="contents[subscription_plan_subtitle]" value="{{ $page->contents['subscription_plan_subtitle'] ?? '' }}" required/>
    </div>
    <div class="form-group">
        <label>Subscription plan details</label>
        <textarea rows="5" class="form-control editor" name="contents[subscription_plan_details]" required>{{ $page->contents['subscription_plan_details'] ?? '' }}</textarea>
    </div>

    @include('admin.template.partials.form_submit')
</div>


