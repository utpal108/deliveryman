@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <section class="ic-faq pt-0">
        <div class="ic-bg-faq"></div>
        <div class="container">
            <h1>{{ $page->contents['page_title'] ?? '' }}</h1>
            <div class="ic-content">
                <span class="secondary-color">
                    {!! $page->contents['page_details'] ?? '' !!}
                </span>
                <div class="ic-tab-dropdown">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        @foreach($faq_groups as $index=>$faq_group)
                            <li class="nav-item">
                                <a class="nav-link {{ ($index==0) ? 'active' : '' }}" id="pills-{{ $index }}-tab" data-toggle="pill" href="#pills-{{ $index }}" role="tab" aria-controls="pills-{{ $index }}" aria-selected="{{ ($index==0) ? true : false }}">{{ $faq_group->group_name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                        @foreach($faq_groups as $index=>$faq_group)
                            <div class="tab-pane fade {{ ($index==0) ? 'show active' : '' }}" id="pills-{{ $index }}" role="tabpanel" aria-labelledby="pills-{{ $index }}-tab">
                                <div class="accordion" id="accordionExampleTwoOne">
                                    @foreach($faq_group->faqs as $sl=>$faq)
                                        <div class="card">
                                            <div class="card-header header-s">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link {{ ($sl==0) ? 'collapsed' : '' }}" type="button" data-toggle="collapse" data-target="#collapse{{ $sl }}" aria-expanded="{{ ($sl==0) ? true : false }}" aria-controls="collapse{{ $sl }}"><i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i>
                                                        {{ $faq->title }}
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapse{{ $sl }}" class="collapse {{ ($sl==0) ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionExampleTwoOne">
                                                <div class="card-body">
                                                    {!! $faq->details !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')

@endsection
