@component('mail::message')
    # From DeliveryMan Contact Message

    <br>
    <b>Title : </b>{{ $message_info['subject'] }}<br/>
    <b>Name : </b>{{ $message_info['first_name'] .' ' .$message_info['last_name'] }}<br/>
    <b>Phone No : </b>{{ $message_info['phone_no'] }}<br/>
    <b>Message : </b>{{ $message_info['message'] }}<br/>

    Thanks,<br/>
    {{ config('app.name') }}
@endcomponent
