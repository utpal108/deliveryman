@extends('admin.layout.app')

@section('page_title','Admin | Edit permission')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\UserRoleController@index') }}">roles</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit role</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" method="post" action="{{ action('Admin\UserRoleController@update',$role->id) }}">
                @csrf
                @method('PUT')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-12">
                            @include('flash::message')

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif

                        <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Role</h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <div class="form-group">
                                                <label>Role Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ $role->name }}" />
                                            </div>

                                        </fieldset>

                                        @foreach($modules as $module)
                                            <fieldset class="demo-switcher-1">
                                                <legend>{{ $module->name }}</legend>
                                                <div class="form-group row">
                                                    @foreach($module->permissions as $permission)
                                                        <div class="col-md-3">
                                                            <label>
                                                                <input type="checkbox" class="checkbox style-0" name="permissions[]" value="{{ $permission->name }}" @if($role->hasPermissionTo($permission->name)){{ 'checked' }} @endif>
                                                                <span>{{ $permission->name }}</span>
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </fieldset>
                                        @endforeach

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    name : {
                        validators : {
                            notEmpty : {
                                message : 'Role name is required'
                            },
                        }
                    }
                }
            });

            // end profile form
            $('div.alert').delay(3000).fadeOut(350);

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
