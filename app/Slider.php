<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable=['title','subtitle','details','details_url','slider_group_id'];

    public function slider_group(){
        return $this->belongsTo('App\SliderGroup','slider_group_id');
    }

    protected $casts=['details_url'=>'array'];
}
