
<div class="modal fade ic-modal" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <div class="bclose-l1"></div>
                <div class="bclose-l2"></div>
            </button>
            <div class="modal-body">
                <div class="content-top text-center">
                    <img src="/images/home/modal-logo.png" alt="">
                    <p>Please register first to use our service. </p>
                </div>
                <form action="{{ action('UserController@store') }}" method="post">
                    @csrf
                    <div class="reg-log-right">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email Address" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone_no" class="form-control" placeholder="Phone No" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                        </div>
                        <div class="form-group">
                            <select name="user_type" class="form-control" required>
                                <option value="customer">User</option>
                                <option value="driver">Driver</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Create AN Account" class="form-control ic-form-btn">
                        </div>
                        <p class="login">Already have an account? <a href=""  data-dismiss="modal" data-toggle="modal" data-target="#login">Log in</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
