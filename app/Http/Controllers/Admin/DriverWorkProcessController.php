<?php

namespace App\Http\Controllers\Admin;

use App\DriverWorkProcess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverWorkProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $driver_work_processes=DriverWorkProcess::orderBy('order')->get();
        return view('admin.driver_work_process.index',compact('driver_work_processes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.driver_work_process.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'details'=>'required',
            'icon'=>'required',
        ]);

        DriverWorkProcess::create($request->all());
        flash('New work process added successfully');
        return redirect()->action('Admin\DriverWorkProcessController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver_work_process=DriverWorkProcess::findOrFail($id);
        return view('admin.driver_work_process.edit',compact('driver_work_process'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'details'=>'required',
            'icon'=>'required',
        ]);
        $driver_work_process=DriverWorkProcess::findOrFail($id);
        $driver_work_process->title=$request->title;
        $driver_work_process->details=$request->details;
        $driver_work_process->icon=$request->icon;
        $driver_work_process->save();
        flash('Work process updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DriverWorkProcess::destroy($id);
        flash('Work process deleted successfully');
        return redirect()->action('Admin\DriverWorkProcessController@index');
    }

    public function manage_order(Request $request)
    {
        $work_processes = json_decode($request->work_processes);
        foreach ($work_processes as $index => $workProcess) {
            $work_process= DriverWorkProcess::findOrFail($workProcess->id);
            $work_process->order = $index + 1;
            $work_process->save();
        }
    }
}
