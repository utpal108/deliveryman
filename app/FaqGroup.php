<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqGroup extends Model
{
    protected $fillable=['group_name'];

    public function faqs(){
        return $this->hasMany('App\Faq','faq_group_id');
    }
}
