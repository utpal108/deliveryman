<!DOCTYPE html>

<html lang="en" class="smart-style-0">
<head>
    <title>DeliveryMan | Forget Password</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,500,700">
    <link rel="shortcut icon" href="/ic_admin/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/ic_admin/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" media="screen, print" href="/ic_admin/css/vendors.bundle.css">
    <link rel="stylesheet" media="screen, print" href="/ic_admin/css/app.bundle.css">
    <link rel="stylesheet" type="text/css" href="/ic_admin/css/login.css">

</head>
<body class=" publicHeader-active animated fadeInDown smart-style-0">

<!-- BEGIN .sa-wrapper -->
<div class="sa-wrapper mt-md-5">

    <div class="sa-page-body mt-md-5">
        <!-- BEGIN .sa-content-wrapper -->
        <div class="sa-content-wrapper mt-md-5">

            <div class="sa-content">

                <div class="main" role="main">

                    <!-- MAIN CONTENT -->
                    <div id="content" class="container padding-top-10">

                        <div class="row">
                            <div class="col-sm-12 col-lg-4 mx-auto">
                                @include('flash::message')
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <div class="well no-padding">
                                    <form action="{{ route('password.email') }}" method="post" id="login-form" class="smart-form client-form">
                                        @csrf
                                        <header>
                                            Reset Password
                                        </header>

                                        <fieldset>
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <section>
                                                <label class="label">E-mail</label>
                                                <label class="input mb-3"> <i class="icon-append fa fa-user"></i>
                                                    <input type="email" name="email" value="{{ old('email') }}" required>
                                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address</b></label>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </section>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn sa-btn-primary">
                                                Reset
                                            </button>
                                        </footer>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END .sa-content-wrapper -->
    </div>

</div>
<!-- END .sa-wrapper -->

<script src="/ic_admin/js/vendors.bundle.js"></script>
<script src="/ic_admin/js/app.bundle.js"></script>

<script>
    $(function () {
        $('#menu1').metisMenu();
    });

    //    For flash message
    $('div.alert').delay(3000).fadeOut(350);
</script>


</body>
</html>
