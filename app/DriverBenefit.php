<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverBenefit extends Model
{
    protected $fillable=['title','details','icon'];
}
