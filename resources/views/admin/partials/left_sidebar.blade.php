
<div class="sa-aside-left">

    <a href="javascript:void(0)"  onclick="SAtoggleClass(this, 'body', 'sa-shortcuts-expanded')" class="sa-sidebar-shortcut-toggle">
        <img src="/ic_admin/img/avatars/sunny.png" alt="" class="online">
        <span>{{ Auth::user()->name }} <span class="fa fa-angle-down"></span></span>
    </a>
    <div class="sa-left-menu-outer">
        <ul class="metismenu sa-left-menu" id="menu1">
            {{--            For Dashboard           --}}
            <li class="{{ request()->is('admin') ? 'active' : '' }}">
                <a class="has-arrow"   href="{{ action('Admin\DashboardController@index') }}" title="Dashboard"><span class="fa fa-lg fa-fw fa-home"></span> <span class="menu-item-parent">Dashboard</span></a>
            </li>

            {{--            For Users            --}}
            <li class="{{ request()->is(['admin/subscribers*','admin/administrators*','admin/team-members*']) ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Users</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/administrators') ? 'active' : '' }}">
                        <a href="{{ action('Admin\UserController@index') }}" title="All Administrators"> All Administrators </a>
                    </li>
                    <li class="{{ request()->is('admin/administrators/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\UserController@create') }}" title="Create Administrator"> Add Administrator </a>
                    </li>
                    <li class="{{ request()->is('admin/subscribers') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriberController@index') }}" title="All Subscribers"> Subscribers </a>
                    </li>
                    <li class="{{ request()->is('admin/subscribers/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriberController@create') }}" title="Create subscriber"> Create Subscriber</a>
                    </li>
                    <li class="{{ request()->is('admin/team-members') ? 'active' : '' }}">
                        <a href="{{ action('Admin\TeamMemberController@index') }}" title="Team Members"> Team Members </a>
                    </li>
                    <li class="{{ request()->is('admin/team-members/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\TeamMemberController@create') }}" title="Create Team Member"> Create Team Member </a>
                    </li>
                </ul>
            </li>

            {{--            For Settings          --}}

            <li class="{{ request()->is(['admin/site-settings*','admin/menu*','admin/widgets*','admin/coverage-areas*','admin/testimonials*','admin/media-coverages*']) ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="FAQ"><span class="fa fa-lg fa-fw fa-list"></span> <span class="menu-item-parent"> Settings</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/site-settings') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SiteSettingController@index') }}" title="site settings"> Site Settings </a>
                    </li>
                    <li class="{{ request()->is('admin/menu') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\MenuController@index') }}" title="menu builder"> Menu Builder </a>
                    </li>
                    <li class="{{ request()->is('admin/widgets') ? 'active' : '' }}">
                        <a href="{{ action('Admin\WidgetController@index') }}" title="widgets"> Widgets </a>
                    </li>
                    <li class="{{ request()->is('admin/coverage-areas') ? 'active' : '' }}">
                        <a href="{{ action('Admin\CoverageAreaController@index') }}" title="Coverage Areas"> Coverage Areas </a>
                    </li>
                    <li class="{{ request()->is('admin/coverage-areas/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\CoverageAreaController@create') }}" title="Create Coverage Area"> Create Coverage Area</a>
                    </li>
                    <li class="{{ request()->is('admin/testimonials') ? 'active' : '' }}">
                        <a href="{{ action('Admin\TestimonialController@index') }}" title="Testimonials"> Testimonials </a>
                    </li>
                    <li class="{{ request()->is('admin/testimonials/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\TestimonialController@create') }}" title="Create Testimonial"> Create Testimonial </a>
                    </li>
                    <li class="{{ request()->is('admin/media-coverages') ? 'active' : '' }}">
                        <a href="{{ action('Admin\MediaPublicationController@index') }}" title="Media Coverages"> Media Coverages </a>
                    </li>
                    <li class="{{ request()->is('admin/media-coverages/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\MediaPublicationController@create') }}" title="Create Media Coverage"> Create Media Coverage </a>
                    </li>
                </ul>
            </li>

            {{--            For Pages           --}}

            <li class="{{ request()->is('admin/pages*') ? 'active' : '' }}"><!-- first-level -->
                <a class="has-arrow"   href="" title="Page"><span class="fa fa-lg fa-fw fa-file"></span> <span class="menu-item-parent"> Page</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/pages') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PageController@index') }}" title="All pages"> All pages</a>
                    </li>
                    <li class="{{ request()->is('admin/pages/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PageController@create') }}" title="Create new page"> Create new page</a>
                    </li>
                </ul>

            </li>

            {{--            For Slider            --}}
            <li class="{{ request()->is('admin/slider*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Slider</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <!-- second-level -->
                    <li class="{{ request()->is('admin/slider-groups') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderGroupController@index') }}" title="Slider groups"> Slider groups </a>
                    </li><!-- second-level -->
                    <li class="{{ request()->is('admin/slider-groups/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderGroupController@create') }}" title="Create slider group"> Create slider group</a>
                    </li><!-- second-level -->
                    <li class="{{ request()->is('admin/sliders') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderController@index') }}" title="Sliders"> Sliders </a>
                    </li>
                    <li class="{{ request()->is('admin/sliders/create') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SliderController@create') }}" title="Create slider"> Create slider </a>
                    </li>
                </ul>

            </li>

            {{--            For Subscriptions            --}}
            <li class="{{ request()->is('admin/subscriptions*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Subscriptions</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/subscriptions') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriptionPlanController@index') }}" title="All Subscribers"> Subscriptions </a>
                    </li>
                    <li class="{{ request()->is('admin/subscriptions/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\SubscriptionPlanController@create') }}" title="Create subscriber"> Create Subscription</a>
                    </li>
                </ul>
            </li>

            {{--            For Customers           --}}
            <li class="{{ request()->is('admin/customers*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Customers</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/customers') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\CustomerController@index') }}" title="All Customers"> Customers </a>
                    </li>
                    <li class="{{ request()->is('admin/customers/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\CustomerController@create') }}" title="Create Customer"> Create Customer</a>
                    </li>
                </ul>
            </li>

            {{--            For Drivers           --}}
            <li class="{{ request()->is(['admin/drivers*','admin/pending-drivers*','admin/driver-benefits*','admin/driver-work-process*']) ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Driver"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Drivers</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/drivers') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\DriverController@index') }}" title="All Drivers"> Drivers </a>
                    </li>
                    <li class="{{ request()->is('admin/drivers/create*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\DriverController@create') }}" title="Create Driver"> Create Driver</a>
                    </li>
                    <li class="{{ request()->is('admin/pending-drivers*') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\PendingDriverController@index') }}" title="Pending Drivers"> Pending Drivers</a>
                    </li>
                    <li class="{{ request()->is('admin/driver-benefits') ? 'active' : '' }}">
                        <a href="{{ action('Admin\DriverBenefitController@index') }}" title="Driver Benefits"> Driver Benefits </a>
                    </li>
                    <li class="{{ request()->is('admin/driver-benefits/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\DriverBenefitController@create') }}" title="Create Driver Benefit"> Create Driver Benefit </a>
                    </li>
                    <li class="{{ request()->is('admin/driver-work-process') ? 'active' : '' }}">
                        <a href="{{ action('Admin\DriverWorkProcessController@index') }}" title="Team Members"> Work processes </a>
                    </li>
                    <li class="{{ request()->is('admin/driver-work-process/create') ? 'active' : '' }}">
                        <a href="{{ action('Admin\DriverWorkProcessController@create') }}" title="Create Team Member"> Add new process </a>
                    </li>
                </ul>
            </li>

            {{--            For Product Delivery           --}}
            <li class="{{ request()->is(['admin/delivery-requests*','admin/received-products*','admin/scheduled-products*','admin/assigned-products*','admin/delivered-products*']) ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Driver"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> Product Delivery</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/delivery-requests') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductDeliveryController@delivery_requests') }}" title="Delivery Requests"> Delivery Requests </a>
                    </li>
                    <li class="{{ request()->is('admin/received-products') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductDeliveryController@received_products') }}" title="Received Products"> Received Products </a>
                    </li>
                    <li class="{{ request()->is('admin/scheduled-products') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductDeliveryController@scheduled_products') }}" title="Scheduled Products"> Scheduled Products </a>
                    </li>
                    <li class="{{ request()->is('admin/assigned-products') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductDeliveryController@assigned_products') }}" title="Assigned Products"> Assigned Products </a>
                    </li>
                    <li class="{{ request()->is('admin/delivered-products') ? 'active' : '' }}">
                        <a   href="{{ action('Admin\ProductDeliveryController@delivered_products') }}" title="Delivered Products"> Delivered Products </a>
                    </li>
                </ul>
            </li>

            {{--            For FAQ           --}}
            <li class="{{ request()->is('admin/faq*') ? 'active' : '' }}">
                <a class="has-arrow"   href="" title="Slider"><span class="fa fa-lg fa-fw fa-sliders"></span> <span class="menu-item-parent"> FAQ</span>
                    <b class="collapse-sign">
                        <em class="fa fa-plus-square-o"></em>
                        <em class="fa fa-minus-square-o"></em>
                    </b>
                </a>
                <ul aria-expanded="true" class="sa-sub-nav collapse">
                    <li class="{{ request()->is('admin/faq-groups') ? 'active' : '' }}">
                        <a href="{{ action('Admin\FAQGroupController@index') }}" title="All Faq Groups"> Faq Groups </a>
                    </li>
                    <li class="{{ request()->is('admin/faq-groups/create*') ? 'active' : '' }}">
                        <a href="{{ action('Admin\FAQGroupController@create') }}" title="Create Faq Group"> Create Faq Group</a>
                    </li>
                    <li class="{{ request()->is('admin/faqs') ? 'active' : '' }}">
                        <a href="{{ action('Admin\FAQController@index') }}" title="All Faqs"> Faqs </a>
                    </li>
                    <li class="{{ request()->is('admin/faqs/create*') ? 'active' : '' }}">
                        <a href="{{ action('Admin\FAQController@create') }}" title="Create Faq"> Create Faq</a>
                    </li>
                </ul>
            </li>


        </ul>
    </div>
    <a href="javascript:void(0)" class="minifyme" onclick="SAtoggleClass(this, 'body', 'minified')">
        <i class="fa fa-arrow-circle-left hit"></i>
    </a>
</div>
