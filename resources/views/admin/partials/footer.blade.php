<footer class="sa-page-footer">
    <div class="d-flex align-items-center w-100 h-100">
        <div class="footer-right ml-auto">
            @copyright reserved by- <span class="footer-txt">DeliveryMan </span> &copy; 2018-{{ date('Y') }}
        </div>
    </div>
</footer>
