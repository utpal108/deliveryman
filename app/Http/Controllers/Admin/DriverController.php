<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Validator;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers=User::where([['user_type','driver'],['status',1]])->get();
        return view('admin.drivers.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'address' => 'required',
            'phone_no' => 'required',
            'working_schedule_from' => 'required|date_format:H:i',
            'working_schedule_to' => 'required|date_format:H:i',
            'status' => 'required|numeric',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);
        $allData['user_type']='driver';

        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }

        User::create($allData);
        flash('New driver added successfully');
        return redirect()->action('Admin\DriverController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver=User::findOrFail($id);
        return view('admin.drivers.edit',compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'name' => 'required',
            'address' => 'required',
            'phone_no' => 'required',
            'working_schedule_from' => 'required|date_format:H:i',
            'working_schedule_to' => 'required|date_format:H:i',
            'status' => 'required|numeric',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $driver=User::findOrFail($id);
        $driver->name=$request->name;
        $driver->email=$request->email;
        $driver->address=$request->address;
        $driver->phone_no=$request->phone_no;
        $driver->working_schedule_from=$request->working_schedule_from;
        $driver->working_schedule_to=$request->working_schedule_to;
        $driver->status=$request->status;
        if ($request->hasFile('profile_image')){
            Storage::delete($driver->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $driver->profile_image=$path;
        }

        if (isset($request->password)){
            $driver->password=bcrypt($request->password);
        }

        $driver->save();
        flash('Driver information updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash('Driver deleted successfully');
        return redirect()->action('Admin\DriverController@index');
    }
}
