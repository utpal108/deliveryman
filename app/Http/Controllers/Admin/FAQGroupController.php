<?php

namespace App\Http\Controllers\Admin;

use App\FaqGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FAQGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faq_groups=FaqGroup::all();
        return  view('admin.faq.faq_group.index',compact('faq_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.faq.faq_group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'group_name' => 'required',
        ]);

        FaqGroup::create($request->all());
        flash('Faq Group Created successfully');
        return redirect()->action('Admin\FAQGroupController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq_group=FaqGroup::findOrFail($id);
        return view('admin.faq.faq_group.edit',compact('faq_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'group_name' => 'required',
        ]);

        $faq_group=FaqGroup::findOrFail($id);
        $faq_group->group_name=$request->group_name;
        $faq_group->save();
        flash('Faq updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FaqGroup::destroy($id);
        flash('Faq group deleted successfully');
        return redirect()->action('Admin\FAQGroupController@index');
    }
}
