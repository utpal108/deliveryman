<?php

namespace App\Http\Controllers\Admin;

use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Validator;
use Illuminate\Validation\Rule;


class UserPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions=\App\Permission::with('module')->get();
        return view('admin.permissions.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules=Module::all();
        return view('admin.permissions.create',compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:permissions',
        ]);

        Permission::create(['name'=>$request->name,'module_id'=>$request->module_id]);
        flash('Permission created successfully');
        return redirect()->action('Admin\UserPermissionController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules=Module::all();
        $permission=Permission::find($id);
        return view('admin.permissions.edit',compact('permission','modules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('permissions')->ignore($id),
            ]
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $permission=Permission::find($id);
        $permission->name=$request->name;
        $permission->module_id=$request->module_id;
        $permission->save();
        flash('Permission Updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::destroy($id);
        flash('Permission deleted successfully');
        return redirect()->action('Admin\UserPermissionController@index');
    }
}
