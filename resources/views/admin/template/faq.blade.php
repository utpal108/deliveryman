
<div class="widget-body">
    <fieldset>
        <legend>
            Form Elements
        </legend>

        <div class="form-group">
            <label>Page title</label>
            <input class="form-control" name="contents[page_title]" value="{{ $page->contents['page_title'] ?? '' }}">
        </div>

        <div class="form-group">
            <label>Page details</label>
            <textarea class="form-control editor" name="contents[page_details]" rows="8">{!! $page->contents['page_details'] ?? '' !!}</textarea>
        </div>
    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


