
<div class="widget-body">
    <fieldset>
        <legend>
            Form data for delivery driver
        </legend>
        <div class="form-group">
            <label>Delivery driver title</label>
            <input type="text" class="form-control" name="contents[delivery_driver][title]" value="{{ $page->contents['delivery_driver']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Delivery driver details</label>
            <textarea rows="5" class="form-control editor" name="contents[delivery_driver][details]" required>{{ $page->contents['delivery_driver']['details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>Delivery driver URL</label>
            <input type="text" class="form-control" name="contents[delivery_driver][url]" value="{{ $page->contents['delivery_driver']['url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Delivery driver image (1920X800)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['delivery_driver']['image'])){{ '/storage/' .$page->contents['delivery_driver']['image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="delivery driver image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="delivery_driver_image" @if(!isset($page->contents['delivery_driver']['image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for driver benefits
        </legend>
        <div class="form-group">
            <label>Driver benefit title</label>
            <input type="text" class="form-control" name="contents[driver_benefit][title]" value="{{ $page->contents['driver_benefit']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Driver benefit subtitle</label>
            <input type="text" class="form-control" name="contents[driver_benefit][subtitle]" value="{{ $page->contents['driver_benefit']['subtitle'] ?? '' }}" />
        </div>

        <legend>
            Form data for driver qualification
        </legend>
        <div class="form-group">
            <label>Driver qualification title</label>
            <input type="text" class="form-control" name="contents[driver_qualification][title]" value="{{ $page->contents['driver_qualification']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Driver qualification subtitle</label>
            <input type="text" class="form-control" name="contents[driver_qualification][subtitle]" value="{{ $page->contents['driver_qualification']['subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Delivery driver details</label>
            <textarea rows="5" class="form-control editor" name="contents[driver_qualification][details]" required>{{ $page->contents['driver_qualification']['details'] ?? '' }}</textarea>
        </div>
        <div class="form-group">
            <label>Driver qualification URL</label>
            <input type="text" class="form-control" name="contents[driver_qualification][url]" value="{{ $page->contents['driver_qualification']['url'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Driver qualification image (430X616)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['driver_qualification']['image'])){{ '/storage/' .$page->contents['driver_qualification']['image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="driver qualification image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="driver_qualification_image" @if(!isset($page->contents['driver_qualification']['image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for delivery process
        </legend>
        <div class="form-group">
            <label>Delivery process title</label>
            <input type="text" class="form-control" name="contents[delivery_process][title]" value="{{ $page->contents['delivery_process']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Delivery process subtitle</label>
            <input type="text" class="form-control" name="contents[delivery_process][subtitle]" value="{{ $page->contents['delivery_process']['subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Delivery process image (1111X563)</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['delivery_process']['image'])){{ '/storage/' .$page->contents['delivery_process']['image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="delivery process image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="delivery_process_image" @if(!isset($page->contents['delivery_process']['image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <legend>
            Form data for our coverage areas
        </legend>
        <div class="form-group">
            <label>Coverage areas title</label>
            <input type="text" class="form-control" name="contents[coverage_areas][title]" value="{{ $page->contents['coverage_areas']['title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Coverage areas subtitle</label>
            <input type="text" class="form-control" name="contents[coverage_areas][subtitle]" value="{{ $page->contents['coverage_areas']['subtitle'] ?? '' }}" required/>
        </div>

    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


