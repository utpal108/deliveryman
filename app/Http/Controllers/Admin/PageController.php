<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\PageTemplate;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages=Page::all();
        return view('admin.pages.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_templates=PageTemplate::all();
        return view('admin.pages.create', compact('page_templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'page_title' => 'required|unique:pages',
        ]);
        $allData=$request->all();
        $allData['slug']=str_replace(' ', '-', $request->slug);
        $page_template=PageTemplate::find($request->template_id);
        if ($page_template->template_name == 'home'){
            if ($request->hasFile('best_fit_image')){
                $path=$request->file('best_fit_image')->store('images');
                $image = Image::make(Storage::get($path))->fit(557,533)->encode();
                Storage::put($path, $image);
                $allData['contents']['best_fit_image']=$path;
            }

            if ($request->hasFile('best_feel_image')){
                $path=$request->file('best_feel_image')->store('images');
                $image = Image::make(Storage::get($path))->fit(210,540)->encode();
                Storage::put($path, $image);
                $allData['contents']['best_feel_image']=$path;
            }

            if ($request->hasFile('best_quality_image')){
                $path=$request->file('best_quality_image')->store('images');
                $image = Image::make(Storage::get($path))->fit(485,335)->encode();
                Storage::put($path, $image);
                $allData['contents']['best_quality_image']=$path;
            }
        }

        else if ( in_array($page_template->template_name, ['default', 'faq', 'supplier_membership', 'top_ranked_suppliers', 'supplier_by_category', 'help_content', 'help_content_reverse'])){
            if ($request->hasFile('slider_image')){

                $path=$request->file('slider_image')->store('images');

                $image = Image::make(Storage::get($path))->resize(1350, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();

                Storage::put($path, $image);
                $allData['contents']['slider_image']=$path;
            }
        }


        Page::create($allData);
        flash('Page Created Successfully');
        return redirect()->action('Admin\PageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_templates=PageTemplate::all();
        $page=Page::with('template')->find($id);
        return view('admin.pages.edit', compact('page','page_templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(), [
            'page_title' => [
                'required',
                Rule::unique('pages')->ignore($id),
            ],
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $allContents=$request->contents;
        $page=Page::with('template')->find($id);

        if ($page->template['template_name'] == 'home'){
            if ($request->hasFile('driver_info_image')){
                $path=$request->file('driver_info_image')->store('images');
                $image = Image::make(Storage::get($path))->resize(540, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
                Storage::put($path, $image);
                $allContents['driver_info']['image']=$path;
                if (isset($page->contents['driver_info']['image'])){
                    Storage::delete($page->contents['driver_info']['image']);
                }
            }
            else{
                $allContents['driver_info']['image']=$page->contents['driver_info']['image'];
            }

            if ($request->hasFile('user_info_image')){
                $path=$request->file('user_info_image')->store('images');
                $image = Image::make(Storage::get($path))->resize(540, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
                Storage::put($path, $image);
                $allContents['user_info']['image']=$path;
                if (isset($page->contents['user_info']['image'])){
                    Storage::delete($page->contents['user_info']['image']);
                }
            }
            else{
                $allContents['user_info']['image']=$page->contents['user_info']['image'];
            }

            if ($request->hasFile('coverage_area_image')){
                $path=$request->file('coverage_area_image')->store('images');
                $image = Image::make(Storage::get($path))->resize(1111, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
                Storage::put($path, $image);
                $allContents['coverage_area']['image']=$path;
                if (isset($page->contents['coverage_area']['image'])){
                    Storage::delete($page->contents['coverage_area']['image']);
                }
            }
            else{
                $allContents['coverage_area']['image']=$page->contents['coverage_area']['image'];
            }
        }

        else if ($page->template['template_name'] == 'delivery_drivers'){
            if ($request->hasFile('delivery_driver_image')){
                $path=$request->file('delivery_driver_image')->store('images');
                $image = Image::make(Storage::get($path))->resize(1920, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
                Storage::put($path, $image);
                $allContents['delivery_driver']['image']=$path;
                if (isset($page->contents['delivery_driver']['image'])){
                    Storage::delete($page->contents['delivery_driver']['image']);
                }
            }
            else{
                $allContents['delivery_driver']['image']=$page->contents['delivery_driver']['image'];
            }

            if ($request->hasFile('driver_qualification_image')){
                $path=$request->file('driver_qualification_image')->store('images');
                $image = Image::make(Storage::get($path))->resize(430, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
                Storage::put($path, $image);
                $allContents['driver_qualification']['image']=$path;
                if (isset($page->contents['driver_qualification']['image'])){
                    Storage::delete($page->contents['driver_qualification']['image']);
                }
            }
            else{
                $allContents['driver_qualification']['image']=$page->contents['driver_qualification']['image'];
            }

            if ($request->hasFile('delivery_process_image')){
                $path=$request->file('delivery_process_image')->store('images');
                $image = Image::make(Storage::get($path))->resize(1111, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
                Storage::put($path, $image);
                $allContents['delivery_process']['image']=$path;
                if (isset($page->contents['delivery_process']['image'])){
                    Storage::delete($page->contents['delivery_process']['image']);
                }
            }
            else{
                $allContents['delivery_process']['image']=$page->contents['delivery_process']['image'];
            }
        }


        $page->page_title=$request->page_title;
        $page->slug=str_replace(' ', '-', $request->slug);
        $page->template_id=$request->template_id;
        $page->contents=$allContents;
        $page->save();
        flash('Page updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);
        flash('Page deleted successfully');
        return redirect()->action('Admin\PageController@index');
    }

    public  function load_template(Request $request){
        $selected_template=PageTemplate::find($request->template_id);
        $page_action= $request->type;
        $page='';
        if (isset($request->page_id)){
            $page=Page::find($request->page_id);
        }

        if ($selected_template){
            return view('admin.template.'.$selected_template->template_name,compact('page_action','page'));
        }
        else{
            return false;
        }

    }
}
