@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <!-- breadcrumb -->
    <div class="ic-breadcrumb">
        <div class="container">
            <div class="inner">
                <h2>{{ ucwords($page->page_title) }}</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">{{ ucwords($page->page_title) }}</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <!-- banner -->
    <div class="ic-delivery-banner" style="background: url({{ '/storage/' .$page->contents['delivery_driver']['image'] ?? '' }}) no-repeat;">
        <div class="container">
            <div class="ic-text">
                <div class="ic-inner">
                    <h1>{{ $page->contents['delivery_driver']['title'] ?? '' }}</h1>
                    {!! $page->contents['delivery_driver']['details'] ?? '' !!}
                    <a href="{{ $page->contents['delivery_driver']['url'] ?? '' }}" class="ic-btn-primary">Apply Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- banner end -->
    <!-- feature -->
    <section class="ic-feature">
        <div class="container">
            <div class="ic-text-top">
                <h2>{{ $page->contents['driver_benefit']['title'] ?? '' }}</h2>
                <h1>{{ $page->contents['driver_benefit']['subtitle'] ?? '' }}</h1>
            </div>
            @include('partials.driver_benefits')
        </div>
    </section>
    <!-- feature end -->
    <!-- deligated -->
    <section class="ic-deligated">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="ic-col">
                        <div class="ic-section-top-text">
                            <h2>{{ $page->contents['driver_qualification']['title'] ?? '' }}</h2>
                            <h1>{{ $page->contents['driver_qualification']['subtitle'] ?? '' }}</h1>
                        </div>
                        {!! $page->contents['driver_qualification']['details'] !!}
                        <a href="{{ $page->contents['driver_qualification']['url'] ?? '' }}" class="ic-btn-primary">Apply Now</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 ic-image">
                    <img src="/storage/{{ $page->contents['driver_qualification']['image'] ?? '' }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- deligated -->
    <!-- silple -->
    <section class="ic-silple">
        <div class="container">
            <div class="ic-text-top">
                <h2>{{ $page->contents['delivery_process']['title'] ?? '' }}</h2>
                <h1>{{ $page->contents['delivery_process']['subtitle'] ?? '' }}</h1>
            </div>
            @include('partials.driver_work_process')
            <div class="ic-image-bottom">
                <img src="/storage/{{ $page->contents['delivery_process']['image'] ?? '' }}" alt="" class=" wow zoomIn" data-wow-delay="1.1s">
            </div>
        </div>
    </section>
    <!-- silple end -->
    <!-- earn money -->
    <section class="top-content ic-earn-money pb-0">
        <div class="container">
            <div class="ic-text-top">
                <h2>{{ $page->contents['coverage_areas']['title'] ?? '' }}</h2>
                <h1>{{ $page->contents['coverage_areas']['subtitle'] }}</h1>
            </div>
            @include('partials.our_coverages')
        </div>
    </section>
@endsection

@section('script')

@endsection
