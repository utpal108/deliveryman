<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        return view('order.index');
    }

    public function create(){
        return view('order.create');
    }

    public function store(Request $request){
        return redirect()->action('OrderController@index');
    }
}
