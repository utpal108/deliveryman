@extends('layout.app')

@section('contents')
    <!-- slider start -->
    @include('partials.slider')
    <!-- slider end -->
    <!-- delivery -->
    <section class="delivery home-section">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="ic-image">
                            <img src="/storage/{{ $page->contents['driver_info']['image'] ?? '' }}" alt="" class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 ic-center-align">
                        <div class="ic-content">
                            <h2>{{ $page->contents['driver_info']['title'] ?? '' }}</h2>
                            <h1>{{ $page->contents['driver_info']['subtitle'] ?? '' }}</h1>
                            {!! $page->contents['driver_info']['details'] ?? '' !!}
                            <a href="{{ $page->contents['driver_info']['url'] ?? '' }}" class="ic-btn-primary">Deliver With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- delivery end -->
    <!-- online retailers -->
    <section class="ic-online-retailers secondary-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <h3>{{ $page->contents['user_info']['title'] ?? '' }}</h3>
                    <h1>{{ $page->contents['user_info']['subtitle'] ?? '' }}</h1>
                    {!! $page->contents['user_info']['details'] ?? '' !!}
                    <a href="{{ $page->contents['user_info']['url'] ?? '' }}" class="ic-btn-primary invert">Learn More</a>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="image">
                        <img src="/storage/{{ $page->contents['user_info']['image'] ?? '' }}" alt="" class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- online retailers end -->
    <!-- five image -->
    <section id="five-image">
        <div class="container">
            <div class="inner">
                @foreach($media_publications as $media_publication)
                    <div class="image">
                        <img src="/storage/{{ $media_publication->media_logo }}" alt="{{ $media_publication->media_name }}">
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- five image end -->
    <!-- slider bottom -->
    <section id="slider-bottom" class="p-0">
        <div class="container">
            <div class="inner">
                <div class="banner">
                    <section id="dg-container" class="dg-container p-0">
                        <div class="dg-wrapper">
                            @foreach($testimonials as $testimonial)
                                <a>
                                    <div class="content">
                                        <i class="fa fa-quote-right" aria-hidden="true"></i>
                                        <p><span>“ {{ $testimonial->title }} ”</span><br/>
                                            {{ $testimonial->details }} </p>
                                        <p><span>{{ $testimonial->user_name }}</span><br/>
                                            {{ $testimonial->designation }}</p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <ol class="button" id="lightButton">
                            <li index="0">
                            <li index="1">
                            <li index="2">
                            <li index="3">
                            <li index="4">
                        </ol>
                        <nav>
                            <span class="dg-prev"></span>
                            <span class="dg-next"></span>
                        </nav>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <!-- slider bottom end -->
    <!-- available deliveryman -->
    <section id="available-deliveryman" class="text-center">
        <div class="container">
            <div class="inner">
                <img src="/storage/{{ $page->contents['coverage_area']['image'] ?? '' }}" alt="coverage area">
                <h1>{{ $page->contents['coverage_area']['title'] ?? '' }}</h1>
                <a href="{{ $page->contents['coverage_area']['url'] ?? '' }}" class="ic-btn-primary">Explore Cities</a>
            </div>
        </div>
    </section>
    <!-- available deliveryman end -->
@endsection

