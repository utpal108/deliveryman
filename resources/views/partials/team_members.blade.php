<div class="row">
    @foreach($team_members as $team_member)
        <div class="col-md-6 col-lg-3">
            <div class="image-single">
                <img src="/storage/{{ $team_member->profile_image }}" alt="profile image">
                <div class="text">
                    <div class="inner">
                        <div class="bg-left"></div>
                        <div class="bg-right"></div>
                        <div class="only-text">
                            <h3>{{ $team_member->name }}</h3>
                            <p>{{ $team_member->designation }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
