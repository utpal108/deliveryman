<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=User::where([['user_type','customer'],['status',1]])->get();
        return view('admin.customers.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'address' => 'required',
            'phone_no' => 'required',
            'status' => 'required|numeric',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);
        $allData['user_type']='customer';

        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }

        User::create($allData);
        flash('New customer added successfully');
        return redirect()->action('Admin\CustomerController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer=User::findOrFail($id);
        return view('admin.customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'name' => 'required',
            'address' => 'required',
            'phone_no' => 'required',
            'status' => 'required|numeric',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $customer=User::findOrFail($id);
        $customer->name=$request->name;
        $customer->email=$request->email;
        $customer->address=$request->address;
        $customer->phone_no=$request->phone_no;
        $customer->status=$request->status;
        if ($request->hasFile('profile_image')){
            Storage::delete($customer->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $customer->profile_image=$path;
        }

        if (isset($request->password)){
            $customer->password=bcrypt($request->password);
        }

        $customer->save();
        flash('Customer information updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash('Customer deleted successfully');
        return redirect()->action('Admin\CustomerController@index');
    }
}
