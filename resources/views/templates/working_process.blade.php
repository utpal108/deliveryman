@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <!-- breadcrumb -->
    <div class="ic-breadcrumb">
        <div class="container">
            <div class="inner">
                <h2>{{ ucwords($page->page_title) }}</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">{{ ucwords($page->page_title) }}</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->

    <div class="ic-other-section">
        <!-- work process -->
        <section class="work-process ic-top-section">
            <div class="container">
                <h2 class="primary-color">{{ $page->contents['work_process_title'] ?? '' }}</h2>
                <h1>{{ $page->contents['work_process_subtitle'] ?? '' }}</h1>
                {!! $page->contents['work_process_details'] ?? '' !!}
            </div>
        </section>
        <!-- work process end -->
        <!-- Mised delivers -->
        @include('partials.take_service')
        <!-- Mised delivers end -->
    </div>
@endsection

@section('script')

@endsection
