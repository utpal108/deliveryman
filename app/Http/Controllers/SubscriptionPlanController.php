<?php

namespace App\Http\Controllers;

use App\SubscriptionPlan;
use App\UserSubscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Validation\Rule;

class SubscriptionPlanController extends Controller
{
    public function store(Request $request){
        if (Auth::check()){
            if (Auth::user()->user_type != 'customer'){
                session()->flash('response_message', ['status'=>'warning','title'=>'Subscription Process Failed !', 'message'=>'Subscription plans are only for users']);
                return redirect()->back();
            }
        }
        else{
            session()->flash('response_message', ['status'=>'warning','title'=>'Subscription Process Failed !', 'message'=>'Please login first']);
            return redirect()->back();
        }

        $validation=Validator::make($request->all(), [
            'subscription_id' => 'exists:subscription_plans,id',
            'subscription_duration' => ["required","numeric", "in:6,12"]
        ]);

        if ($validation->fails()){
            session()->flash('response_message', ['status'=>'warning','title'=>'Subscription Process Failed !', 'message'=>$validation->errors()->first()]);
            return redirect()->back();
        }

        $subscription_plan=SubscriptionPlan::findOrFail($request->subscription_id);

        $allData=$request->all();
        $allData['user_id']=Auth::id();
        $allData['subscription_id']=$request->subscription_id;
        $allData['expire_date']=Carbon::now()->addMonths($request->subscription_duration);
        $allData['price']=($subscription_plan->price*$request->subscription_duration);
        $allData['status']=1;
        UserSubscription::create($allData);

        session()->flash('response_message', ['status'=>'success','title'=>'Subscription Process Succeed !', 'message'=>'You are successfully subscribed']);
        return redirect()->back();
    }
}
