<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Delivery Man @yield('page_title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link href="/ic_admin/img/favicon/favicon.png" rel="icon">

    <!-- fonts -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="/css/style.css" rel="stylesheet">

</head>
<body id="body">
<!--navbar -->
<header>
    <div class="container">
        @include('partials.menu')
    </div>
</header>
<!-- navbar -->

<!-- register modal -->
@include('partials.register_modal')

<!-- login modal -->
@include('partials.login_modal')

<main>
    @yield('contents')
</main>
<!-- Required JavaScript Libr
aries -->
@include('partials.footer')

<!-- scroll top -->
<div id="ic-scroll-top">
    <a href="#body">Top</a>
</div>
<!-- scroll top end-->

<!-- preloader -->
<div id="overlayer">
    <span class="loader">
      <span class="loader-inner"></span>
    </span>
</div>

<!-- preloader -->
<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="/js/plugins.min.js"></script>
<script src="/js/custom.min.js"></script>
<script>
    var response_message ="{{ (session('response_message')) ? 'present' : 'absent'  }}";
    if (response_message != 'absent'){
        swal("{{ (isset(session('response_message')['title'])) ? session('response_message')['title'] : ''  }}", "{{ (isset(session('response_message')['message'])) ? session('response_message')['message'] : ''  }}","{{ (isset(session('response_message')['status'])) ? session('response_message')['status'] : ''  }}")
    }
</script>
@yield('script')
</body>
</html>

