@extends('admin.layout.app')

@section('page_title','Admin | Team Members')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\TeamMemberController@index') }}">Team Members</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard <span>> Team Members</span></h1>
            </div>
        </div>
        <div>
            <div>

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-12">
                        @include('flash::message')
                        <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <div class="widget-header">
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>All team members </h2>
                                    </div>
                                    <div class="widget-toolbar">
                                        <!-- add: non-hidden - to disable auto hide -->
                                    </div>
                                </header>
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body p-0">

                                        <div class="col-lg-10 offset-1">

{{--                                            <h6>Nestable List #3 (with drag handle)</h6>--}}

                                            <div class="dd" id="nestable3" style="max-width: 900px">
                                                <ol class="dd-list mb-5 mt-5">
                                                    @foreach($team_members as $team_member)
                                                        <li class="dd-item dd3-item" data-id="{{ $team_member->id }}">
                                                            <div class="dd-handle dd3-handle">
                                                                ''
                                                            </div>
                                                            <div class="dd3-content">
                                                                {{ $team_member->name }}

                                                                <span class="pull-right">
                                                                    <a class="btn btn-success btn-xs" href="{{ action('Admin\TeamMemberController@edit',$team_member->id) }}">Edit</a>
                                                                    <form action="{{ action('Admin\TeamMemberController@destroy',$team_member->id) }}" method="post" style="display: inline">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button class="btn btn-danger btn-xs" name="remove_slider_group" type="submit">Delete</button>
                                                                    </form>
                                                                </span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- end widget content -->

                                    <!-- Bootstrap Modal -->
                                    <div class="modal fade" id="confirm">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h2 class="modal-title">Delete team member</h2>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    <h4>Are you sure ?</h4>
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <button type="submit" class="btn btn-danger" id="delete">Yes</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>
                        <!-- WIDGET END -->

                    </div>

                    <!-- end row -->

                    <!-- end row -->

                </section>
                <!-- end widget grid -->

            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- Jquery Toster--}}
    <script src="/ic_admin/js/jquery.toaster.js"></script>
    <script>
        $('#nestable3').nestable({ maxDepth :1 });

        $('.dd').on('change', function (e) {
            var order= JSON.stringify($('.dd').nestable('serialize'));
            $.post('/admin/order-members', {
                member_orders: JSON.stringify($('.dd').nestable('serialize')),
                _token: '{{ csrf_token() }}'
            }, function (data) {
                $.toaster('Order Updated Successfully', 'Success', 'success');
            });
        });

        function deleteSliderGroup() {
            $('#myModal').modal("show");
        }

        $('button[name="remove_slider_group"]').on('click', function(e) {
            var $form = $(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '#delete', function(e) {
                    $form.trigger('submit');
                });
        });
    </script>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
