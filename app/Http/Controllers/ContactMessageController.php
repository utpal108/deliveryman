<?php

namespace App\Http\Controllers;

use App\Mail\UserContact;
use App\Rules\Captcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Validation\Rule;


class ContactMessageController extends Controller
{
    public function store(Request $request){
        $validation=Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required',
            'subject' => 'required',
            'message' => 'required',
//            'g-recaptcha-response' => new Captcha(),
        ]);

        if ($validation->fails()){
            session()->flash('response_message', ['status'=>'warning','title'=>'Message sent Failed !', 'message'=>$validation->errors()->first()]);
            return redirect()->back();
        }

        $allData=$request->all();
        $allData['mail_title']='User contact message';
        Mail::to('admin@deliveryman.com')->send(new UserContact($allData));
        session()->flash('response_message', ['status'=>'success','title'=>'Message sent successfully', 'message'=>'Thanks for contacting with us']);
        return redirect()->back();
    }
}
