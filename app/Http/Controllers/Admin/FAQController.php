<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\FaqGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs=Faq::all();
        return view('admin.faq.index',compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faq_groups=FaqGroup::all();
        return view('admin.faq.create',compact('faq_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'faq_group_id' => 'exists:faq_groups,id',
            'title' => 'required',
            'details' => 'required',
        ]);

        Faq::create($request->all());
        flash('Faq created successfully');
        return redirect()->action('Admin\FAQController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq=Faq::findOrFail($id);
        $faq_groups=FaqGroup::all();
        return view('admin.faq.edit',compact('faq','faq_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'faq_group_id' => 'exists:faq_groups,id',
            'title' => 'required',
            'details' => 'required',
        ]);
        $faq=Faq::findOrFail($id);
        $faq->faq_group_id=$request->faq_group_id;
        $faq->title=$request->title;
        $faq->details=$request->details;
        $faq->save();
        flash('Faq updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::destroy($id);
        flash('Faq deleted successfully');
        return redirect()->action('Admin\FAQController@index');
    }
}
