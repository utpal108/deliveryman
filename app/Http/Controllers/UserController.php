<?php

namespace App\Http\Controllers;

use App\OrderDetails;
use App\ProductOrder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;


class UserController extends Controller
{
    public function store(Request $request){
        $validation=Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'phone_no' => 'required',
            'user_type'=> ["required" , "in:customer,driver"]
        ]);

        if ($validation->fails()){
            session()->flash('response_message', ['status'=>'warning','title'=>'Registration Failed !', 'message'=>$validation->errors()->first()]);
            return redirect()->back();
        }

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);
        if ($request->user_type == 'driver'){
            $allData['status']= 2;
        }
        $user=User::create($allData);
        session()->flash('response_message', ['status'=>'success','title'=>'Registered successfully', 'message'=>'Please verify your email before login']);
        Auth::loginUsingId($user->id,true);
        return redirect()->action('Auth\VerificationController@resend');
    }

    public function login(){
        return view('user.login');
    }

    public function register(){
        if (Auth::check()){
            return redirect('/');
        }
        else{
            return view('user.register');
        }
    }

    public function authenticate(Request $request){

        $validation=Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password],$request->remember_token)){
            if (Auth::user()->status != 1){
                if (Auth::user()->status == 0){
                    session()->flash('response_message', ['status'=>'warning','title'=>'Please wait for admin approval']);
                }
                else{
                    session()->flash('response_message', ['status'=>'warning','title'=>'You are not permitted', 'message'=>'Please use valid username or password']);
                }
                Auth::logout();
            }
        }
        else{
            session()->flash('response_message', ['status'=>'warning','title'=>'You are not permitted', 'message'=>'Please use valid username or password']);
        }

        return redirect()->back();
    }

    public function logout(){
        Auth::logout();
        return redirect()->back();
    }

    public function profile(){
        return view('user.profile');
    }

    public function edit_profile(){
        $user=Auth::user();
        return view('user.edit_profile',compact('user'));
    }

    public function update_profile(Request $request){
        $user=Auth::user();
        $validation=Validator::make($request->all(), [
            // Ignore user id
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $user->name=$request->name;
        $user->email=$request->email;
        $user->country_code=$request->country_code;
        $user->phone_no=$request->phone_no;
        $user->profession=$request->profession;
        $user->address=$request->address;
        if (isset($request->password)){
            $user->password=bcrypt($request->password);
        }

        if ($request->hasFile('profile_image')){
            Storage::delete($user->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $user->profile_image=$path;
        }

        if ($request->hasFile('banner_image')){
            Storage::delete($user->banner_image);
            $path=$request->file('banner_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(1110, 250)->encode();
            Storage::put($path, $image);
            $user->banner_image=$path;
        }
        $user->save();
        flash('Profile updated successfully');
        return redirect()->back();
    }

    public function notifications(){
        return view('user.notifications');
    }

    public function schedule_request(){
        return view('user.schedule_request');
    }

    public function select_schedule(){
        return view('user.select_schedule');
    }

    public function set_schedule(Request $request){
        return redirect()->action('UserController@schedule_request');
    }

    public function payment_info(){
        return view('user.payment_info');
    }

    public function update_payment_info(Request $request){
        return redirect()->back();
    }
}
