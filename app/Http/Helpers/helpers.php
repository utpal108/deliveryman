<?php

use App\SiteSetting;

if (! function_exists('setting')) {
        function setting($key = null)
        {
            if (!is_null($key)){
                $setting=SiteSetting::where('key',$key)->first();
                if (!is_null($setting)){
                    return $setting->value;
                }
                else{
                    return null;
                }
            }
            else{
                return null;
            }

        }
    }

