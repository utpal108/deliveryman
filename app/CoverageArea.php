<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoverageArea extends Model
{
    protected $fillable=['area_name','address','phone_no','email','area_image'];
}
