
<div class="widget-body">
    <fieldset>
        <legend>
            Form data for about us
        </legend>

        <div class="form-group">
            <label>About us title</label>
            <input type="text" class="form-control" name="contents[about_us_title]" value="{{ $page->contents['about_us_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>About us subtitle</label>
            <input type="text" class="form-control" name="contents[about_us_subtitle]" value="{{ $page->contents['about_us_subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>About us details</label>
            <textarea rows="5" class="form-control editor" name="contents[about_us_details]" required>{{ $page->contents['about_us_details'] ?? '' }}</textarea>
        </div>

        <legend>
            Form data for team member
        </legend>
        <div class="form-group">
            <label>Team member title</label>
            <input type="text" class="form-control" name="contents[team_member_title]" value="{{ $page->contents['team_member_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Team member subtitle</label>
            <input type="text" class="form-control" name="contents[team_member_subtitle]" value="{{ $page->contents['team_member_subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Team member details</label>
            <textarea rows="5" class="form-control editor" name="contents[team_member_details]" required>{{ $page->contents['team_member_details'] ?? '' }}</textarea>
        </div>

    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


