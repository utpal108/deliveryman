<?php

namespace App\Providers;

use App\CoverageArea;
use App\DriverBenefit;
use App\DriverWorkProcess;
use App\FaqGroup;
use App\MediaPublication;
use App\Menu;
use App\Slider;
use App\SubscriptionPlan;
use App\TeamMember;
use App\Testimonial;
use App\Widget;
use Illuminate\Support\ServiceProvider;

class ViewContentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //	    For Menu
//        view()->composer('partials.menu', function($view) {
//            $items=Menu::display('admin');
//            $view->with(['items'=>$items]);
//        });

        //        Subscription plans
        view()->composer('templates.subscription_plan', function($view) {
            $subscription_plans=SubscriptionPlan::where('status',1)->get();
            $view->with(['subscription_plans'=>$subscription_plans]);
        });

        //        Sliders
        view()->composer('partials.slider', function($view) {
            $sliders=Slider::all();
            $view->with(['sliders'=>$sliders]);
        });

        //        Faqs
        view()->composer('templates.faq', function($view) {
            $faq_groups=FaqGroup::all();
            $view->with(['faq_groups'=>$faq_groups]);
        });

        //        Coverage Areas
        view()->composer(['templates.contact','partials.our_coverages'], function($view) {
            $coverage_areas=CoverageArea::all();
            $view->with(['coverage_areas'=>$coverage_areas]);
        });

        //        Testimonials and Media Coverage
        view()->composer('templates.home', function($view) {
            $testimonials=Testimonial::all();
            $media_coverages=MediaPublication::all();
            $view->with(['testimonials'=>$testimonials, 'media_publications'=>$media_coverages]);
        });

        //        Footer
        view()->composer('partials.footer', function($view) {
            $footer=Widget::where('name','Footer')->first();
            $view->with(['footer'=>$footer]);
        });

        //        Team Members
        view()->composer('partials.team_members', function($view) {
            $team_members=TeamMember::orderBy('order')->get();
            $view->with(['team_members'=>$team_members]);
        });

        //        Take Service Component
        view()->composer('partials.take_service', function($view) {
            $service=Widget::where('name','Take Service')->first();
            $view->with(['service'=>$service]);
        });

        //        Drive With Us
        view()->composer('partials.drive_with_us', function($view) {
            $drive_with_us=Widget::where('name','Drive With Us')->first();
            $view->with(['drive_with_us'=>$drive_with_us]);
        });

        //        Driver work process
        view()->composer('partials.driver_work_process', function($view) {
            $driver_work_processes=DriverWorkProcess::orderBy('order')->get();
            $view->with(['driver_work_processes'=>$driver_work_processes]);
        });

        //        Driver benefits
        view()->composer('partials.driver_benefits', function($view) {
            $driver_benefits=DriverBenefit::orderBy('order')->get();
            $view->with(['driver_benefits'=>$driver_benefits]);
        });
    }
}
