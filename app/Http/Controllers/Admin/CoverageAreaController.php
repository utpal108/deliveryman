<?php

namespace App\Http\Controllers\Admin;

use App\CoverageArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CoverageAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coverage_areas=CoverageArea::all();
        return view('admin.coverage_area.index',compact('coverage_areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coverage_area.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'area_name'=>'required',
            'address'=>'required',
            'phone_no'=>'required',
            'email'=>'required',
        ]);

        $allData=$request->all();
        if ($request->hasFile('area_image')){
            $path=$request->file('area_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(350, 234)->encode();
            Storage::put($path, $image);
            $allData['area_image']=$path;
        }
        CoverageArea::create($allData);
        flash('Coverage area created successfully');
        return redirect()->action('Admin\CoverageAreaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coverage_area=CoverageArea::findOrFail($id);
        return view('admin.coverage_area.edit',compact('coverage_area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'area_name'=>'required',
            'address'=>'required',
            'phone_no'=>'required',
            'email'=>'required',
        ]);
        $coverage_area=CoverageArea::findOrFail($id);
        $coverage_area->area_name=$request->area_name;
        $coverage_area->address=$request->address;
        $coverage_area->phone_no=$request->phone_no;
        $coverage_area->email=$request->email;

        if ($request->hasFile('area_image')){
            Storage::delete($coverage_area->area_image);
            $path=$request->file('area_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(350, 234)->encode();
            Storage::put($path, $image);
            $coverage_area->area_image=$path;
        }
        $coverage_area->save();
        flash('Coverage area updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coverage_area=CoverageArea::findOrFail($id);
        Storage::delete($coverage_area->area_image);
        CoverageArea::destroy($id);
        flash('Coverage area deleted successfully');
        return redirect()->action('Admin\CoverageAreaController@index');
    }
}
