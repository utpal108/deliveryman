<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('ecommerce_site');
            $table->string('order_no');
            $table->string('approx_receiving_date')->nullable();
            $table->string('receiving_date')->nullable();
            $table->unsignedBigInteger('received_by')->nullable();
            $table->foreign('received_by')->references('id')->on('users')->onDelete('cascade');
            $table->text('delivery_address');
            $table->string('phone_no');
            $table->string('schedule_date')->nullable();
            $table->string('schedule_time_from')->nullable();
            $table->string('schedule_time_to')->nullable();
            $table->string('delivered_to')->nullable();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('driver_assigned_by')->nullable();
            $table->foreign('driver_assigned_by')->references('id')->on('users')->onDelete('cascade');
            $table->string('delivery_date')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_orders');
    }
}
