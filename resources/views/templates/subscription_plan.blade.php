@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <!-- breadcrumb -->
    <div class="ic-breadcrumb">
        <div class="container">
            <div class="inner">
                <h2>{{ ucwords($page->page_title) }}</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">{{ ucwords($page->page_title) }}</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <div class="subcription-plan ic-other-section coverage-area" id="app">
        <!-- top content start -->
        <section class="ic-top-section">
            <div class="container">
                <div class="inner">
                    <h2 class="primary-color">{{ $page->contents['subscription_plan_title'] ?? '' }}</h2>
                    <h1>{{ $page->contents['subscription_plan_subtitle'] ?? '' }}</h1>
                    {!! $page->contents['subscription_plan_details'] ?? '' !!}
                    <div class="monthly-annualy">
                        <div class="monthly">
                            <h6>Monthly</h6>
                        </div>
                        <div class="switch-field">
                            <input type="radio" id="radio-three" name="subscription_duration" value="6" v-model="subscription_duration" checked/>
                            <label for="radio-three"></label>
                            <input type="radio" id="radio-four" name="subscription_duration" value="12" v-model="subscription_duration" />
                            <label for="radio-four"></label>
                        </div>
                        <div class="annualy">
                            <h6>Anually<span>(Save 20%)</span></h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="plan">
                <div class="wrapper">
                    <div class="inner">
                        @foreach($subscription_plans as $subscription_plan)
                            <div class="single-plan">
                                <div class="single-plan-inner">
                                    <div class="top">
                                        <div class="icon">
                                            <i class="fa {{ $subscription_plan->icon }}" aria-hidden="true"></i>
                                        </div>
                                        <h2>{{ $subscription_plan->title }}</h2>
                                        <h3>${{ $subscription_plan->regular_price }}</h3>
                                        <h1>${{ $subscription_plan->discount_price }}</h1>
                                    </div>
                                    <div class="bottom">
                                        {!! $subscription_plan->description !!}
                                        <form action="{{ action('SubscriptionPlanController@store') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="subscription_id" value="{{ $subscription_plan->id }}">
                                            <input type="hidden" v-model="subscription_duration" name="subscription_duration" value="6">
                                            <button type="submit">Start Now</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <!-- top content end -->
        <!-- last content -->
        @include('partials.drive_with_us')
        <!-- last content end -->
    </div>
@endsection

@section('script')
    <script src="/js/app.js"></script>
@endsection
