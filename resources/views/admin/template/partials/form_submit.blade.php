<div class="form-actions">
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-default" type="submit">
                <i class="fa fa-send"></i>
                <span id="form-action">
                    @if(isset($page_action) && ($page_action == 'edit'))
                        Update
                    @else
                        Create
                    @endif
                </span>
            </button>
        </div>
    </div>
</div>
