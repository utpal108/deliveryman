<?php

namespace App\Http\Controllers\Admin;

use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;


class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers=Subscriber::all();
        return view('admin.subscriber.index',compact('subscribers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subscriber.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'email' => 'required|unique:subscribers',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }

        Subscriber::create($request->all());
        flash('Subscriber created successfully');
        return redirect()->action('Admin\SubscriberController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscriber=Subscriber::find($id);
        return view('admin.subscriber.edit',compact('subscriber'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            // Ignore user id
            'email' => [
                'required',
                Rule::unique('subscribers')->ignore($id),
            ],
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $subscriber=Subscriber::find($id);
        $subscriber->email=$request->email;
        $subscriber->save();
        flash('Subscriber updated successfully');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subscriber::destroy($id);
        flash('Subscriber deleted successfully');
        return redirect()->action('Admin\SubscriberController@index');
    }
}
