
<div class="modal fade ic-modal" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <div class="bclose-l1"></div>
                <div class="bclose-l2"></div>
            </button>
            <div class="modal-body">
                <div class="content-top text-center">
                    <img src="/images/home/modal-logo.png" alt="">
                    <p>Please login first. </p>
                </div>
                <form action="{{ action('UserController@authenticate') }}" method="post">
                    @csrf
                    <div class="reg-log-right">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="email" name="email" class="form-control" placeholder="Email Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="log into my account" class="form-control ic-form-btn mb-0">
                        </div>
                        <div class="text">
                            <p class="forget-password"><a href="/password/reset">Forgot your password?</a></p>
                            <h2>Don't have a free account yet?</h2>
                            <p class="create-account"><a href=""  data-dismiss="modal" data-toggle="modal" data-target="#register" class="ic-form-btn">Crearte An account</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
