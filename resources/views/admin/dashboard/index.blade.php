@extends('admin.layout.app')

@section('page_title','Admin | Dashboard')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div id="content">
            <div class="row hidden-mobile">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h1 class="page-title text-blue-dark text-center well">
                        DeliveryMan Dashboard<br>
                        <small class="text-primary">Manage your settings</small>
                    </h1>
                </div>
                <!-- end col -->
            </div>

            <!-- widget grid -->
            <section id="widget-grid" class="">
                <!-- row -->
                <div class="row">
                    <!-- a blank row to get started -->
                    <div class="col-sm-6">
                        <!-- your contents here -->
                        <div class="well text-center well-layouts">
                            <h5>
                                <strong>Customers</strong> <br>
                                <small>{{ $customers }}</small>
                            </h5>
                            <a class="btn btn-success" href="{{ action('Admin\CustomerController@index') }}" style="text-decoration: none; color: white">Find All</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <!-- your contents here -->
                        <div class="well text-center well-layouts">
                            <h5>
                                <strong>Drivers</strong> <br>
                                <small>{{ $drivers }}</small>
                            </h5>
                            <a class="btn btn-success" href="{{ action('Admin\DriverController@index') }}" style="text-decoration: none; color: white">Find All</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- a blank row to get started -->
                    <div class="col-sm-6">
                        <!-- your contents here -->
                        <div class="well text-center well-layouts">
                            <h5>
                                <strong>Team Members</strong> <br>
                                <small>{{ $team_members }}</small>
                            </h5>
                            <a class="btn btn-success" href="{{ action('Admin\TeamMemberController@index') }}" style="text-decoration: none; color: white">Find All</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <!-- your contents here -->
                        <div class="well text-center well-layouts">
                            <h5>
                                <strong>Coverage Areas</strong> <br>
                                <small>{{ $coverage_areas }}</small>
                            </h5>
                            <a class="btn btn-success" href="{{ action('Admin\CoverageAreaController@index') }}" style="text-decoration: none; color: white">Find All</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- a blank row to get started -->
                    <div class="col-sm-6">
                        <!-- your contents here -->
                        <div class="well text-center well-layouts">
                            <h5>
                                <strong>Product Requests</strong> <br>
                                <small>{{ $delivery_requests }}</small>
                            </h5>
                            <a class="btn btn-success" href="{{ action('Admin\ProductDeliveryController@delivery_requests') }}" style="text-decoration: none; color: white">Find All</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <!-- your contents here -->
                        <div class="well text-center well-layouts">
                            <h5>
                                <strong>Deliver Products</strong> <br>
                                <small>{{ $delivered_products }}</small>
                            </h5>
                            <a class="btn btn-success" href="{{ action('Admin\ProductDeliveryController@delivered_products') }}" style="text-decoration: none; color: white">Find All</a>
                        </div>
                    </div>
                </div>

            </section>
            <!-- end widget grid -->


        </div>
    </div>
@endsection

@section('script')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
