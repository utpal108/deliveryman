@extends('admin.layout.app')

@section('page_title','Admin | Edit slider')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\SliderGroupController@create') }}">Create slider</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit slider</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">
                @include('flash::message')
                <form id="sliderGroup" action="{{ action('Admin\SliderController@update',$slider->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Slider </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Elements
                                            </legend>
                                            <div class="form-group">
                                                <label>Slider Title</label>
                                                <input type="text" class="form-control" name="title" value="{{ $slider->title }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Slider Subtitle</label>
                                                <input type="text" class="form-control" name="subtitle" value="{{ $slider->subtitle }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Details</label>
                                                <input type="text" class="form-control" name="details" value="{{ $slider->details }}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Details URL</label>
                                                <div class="row" style="margin: 0px 1px">
                                                    <input name="details_url[title]" value="{{ $slider->details_url['title'] }}" class="form-control col-lg-6" placeholder="Title" type="text">
                                                    <input name="details_url[url]" value="{{ $slider->details_url['url'] }}" class="form-control col-lg-6" placeholder="URL" type="url">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Slider Group</label>
                                                <select name="slider_group_id" class="form-control">
                                                    @foreach($slider_groups as $slider_group)
                                                        <option value="{{ $slider_group->id }}" @if($slider->slider_group_id == $slider_group->id){{ 'selected' }} @endif>{{ $slider_group->group_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

        })
    </script>
    {{-- For Flash message--}}
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
