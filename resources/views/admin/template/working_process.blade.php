
<div class="widget-body">
    <fieldset>
        <legend>
            Form data for working process
        </legend>

        <div class="form-group">
            <label>About us title</label>
            <input type="text" class="form-control" name="contents[work_process_title]" value="{{ $page->contents['work_process_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>About us subtitle</label>
            <input type="text" class="form-control" name="contents[work_process_subtitle]" value="{{ $page->contents['work_process_subtitle'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>About us details</label>
            <textarea rows="5" class="form-control editor" name="contents[work_process_details]" required>{{ $page->contents['work_process_details'] ?? '' }}</textarea>
        </div>

    </fieldset>

    @include('admin.template.partials.form_submit')
</div>


