<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    public function index(){
        return view('delivery.index');
    }

    public function assign_works(){
        return view('delivery.assign_works');
    }

    public function deliver_product(Request $request){
        return redirect()->action('DeliveryController@assign_works');
    }
}
