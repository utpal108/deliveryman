<?php

namespace App\Http\Controllers\Admin;

use App\ContactMessage;
use App\Mail\UserContact;
use App\Rules\Captcha;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Validation\Rule;


class ContactMessageController extends Controller
{
    public function store(Request $request){
        $validator=Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'g-recaptcha-response' => new Captcha(),
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $allData=$request->all();
        $allData['mail_title']='User contact message';
        ContactMessage::create($allData);
        Mail::to('contact@unitycreation.com')->send(new UserContact($allData));
        session()->flash('success_message', 'Message send successfully');
        return redirect()->back();
    }
}
