@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('style')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
         /*CSS For error message*/
        .invalid-feedback {
            display: none;
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #e3342f;
        }

    </style>
@endsection

@section('contents')
    <!-- breadcrumb -->
    <div class="ic-breadcrumb">
        <div class="container">
            <div class="inner">
                <h2>Contact Us</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="/contact">Contact Us</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->
    <div class="contact ic-other-section">
        <!-- top content start -->
        <section class="ic-top-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6">

                        <div class="inner">
                            <h2 class="primary-color">Contact Us</h2>
                            <h1>Supports & General Inquiries</h1>
                            @foreach($coverage_areas as $coverage_area)
                                <div class="ic-block">
                                    <h3>{{ $coverage_area->area_name }}</h3>
                                    <p>{{ $coverage_area->address }}</p>
                                    <p class="phone"><span class="secondary-color">Phone :</span> {{ $coverage_area->phone_no }}</p>
                                    <p class="email"><span class="secondary-color">Email : </span>{{ $coverage_area->email }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="inner second-col">
                            <h2 class="primary-color">Message Us</h2>
                            <h1>Ask Question To Know About Services</h1>
                            <form action="{{ action('ContactMessageController@store') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <input type="text" name="first_name" class="form-control" placeholder="First Name" required>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <input type="text" name="last_name" class="form-control" placeholder="Last Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email Address" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone_no" class="form-control" placeholder="Phone Number" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write Something Abut To Know" required></textarea>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <button type="submit" class="ic-btn-primary">Submit Now</button>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
        </section>
        <!-- top content end -->
        <!-- last content -->
        <section class="map p-0">
            <div id="map-canvas" class="m-0 p-0">
                <div id="map" style="width:100%; height:450px ;border:0px "></div>
                <!-- Replace the value of the key parameter with your own API key. -->
            </div>
        </section>
        <!-- last content end -->
    </div>
@endsection

@section('script')
    <script>
        var map;
        function initMap() {
            var location = {lat: parseFloat({{ setting('current_lat') }}), lng: parseFloat({{ setting('current_long') }})};
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 15, center: location});
            var marker = new google.maps.Marker({position: location, map: map});
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNrASOuKNnXr4a5rEqDC_8fo_isduYSeU&callback=initMap">
    </script>
@endsection
