<ul class="ic-dropdown-menu">
    @foreach($items as $item)
        <li class="{{ request()->is($item->url) ? 'active' : '' }}">
            <a class="dropdown-item" href="/{{ $item->url }}">{{ $item->title }}</a>
            @if(!$item->children->isEmpty())
                @include('partials.menu-items', ['items' => $item->children])
            @endif
        </li>
    @endforeach
</ul>
