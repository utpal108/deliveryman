@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page->page_title }}</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="sourcing py-0">
            <div class="container">
                <div class="full-content">
                    {!! $page->contents['page_content'] !!}
                </div>
            </div>
        </section>
    </main>
@endsection
