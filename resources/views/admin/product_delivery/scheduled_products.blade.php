@extends('admin.layout.app')

@section('page_title','Admin | Scheduled Products')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ProductDeliveryController@scheduled_products') }}">Scheduled Products</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard <span>> Scheduled Products</span></h1>
            </div>
        </div>
        <div>
            <div>

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-12">
                        @include('flash::message')
                        <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <div class="widget-header">
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>All scheduled products </h2>
                                    </div>
                                    <div class="widget-toolbar">
                                        <!-- add: non-hidden - to disable auto hide -->
                                    </div>
                                </header>
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body p-0">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone" class="text-center">ID</th>
                                                <th data-class="expand" class="text-center"> Customer Name</th>
                                                <th data-class="expand" class="text-center"> Phone No</th>
                                                <th data-class="expand" class="text-center"> Ecommerce Site</th>
                                                <th data-class="expand" class="text-center"> Order No</th>
                                                <th data-class="expand" class="text-center"> Schedule Date</th>
                                                <th data-class="expand" class="text-center"> Schedule Time</th>
                                                <th class="text-center"><i class="fa fa-fw fa-calendar text-blue hidden-md hidden-sm hidden-xs"></i> Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($scheduled_products as $index=>$scheduled_product)
                                                <tr class="text-center">
                                                    <td>{{ $index+1 }}</td>
                                                    <td>{{ $scheduled_product->customer['name'] }}</td>
                                                    <td>{{ $scheduled_product->customer['phone_no']  }}</td>
                                                    <td>{{ $scheduled_product->ecommerce_site }}</td>
                                                    <td>{{ $scheduled_product->order_no }}</td>
                                                    <td>{{ $scheduled_product->schedule_date }}</td>
                                                    <td>{{ $scheduled_product->schedule_time_from . ' - ' .$scheduled_product->schedule_time_to }}</td>
                                                    <td>
                                                        <button class="btn btn-success btn-xs" type="button" onclick="deleteSliderGroup({{ $scheduled_product->id }})">Assign Driver</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->

                                    <!-- Bootstrap Modal -->

                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Assign Driver</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                        ×
                                                    </button>
                                                </div>
                                                <form action="{{ action('Admin\ProductDeliveryController@assign_driver') }}" method="post">
                                                    @csrf
                                                    <div class="modal-body">

                                                        <div class="row">
                                                            <div class="col-12">
                                                                <input type="hidden" id="order_id" name="order_id" value="">
                                                                <div class="form-group">
                                                                    <label for="driver"> Select Driver</label>
                                                                    <select class="form-control" id="driver_id" name="driver_id"></select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                                            Cancel
                                                        </button>
                                                        <button type="submit" class="btn sa-btn-primary">
                                                            Assign
                                                        </button>
                                                    </div>
                                                </form>

                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>
                        <!-- WIDGET END -->

                    </div>

                    <!-- end row -->

                    <!-- end row -->

                </section>
                <!-- end widget grid -->

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteSliderGroup(orderId) {
            $('#order_id').val(orderId);
            $.ajax({
                type:'GET',
                url:'/admin/get-drivers/'+orderId,
                data:{order_id: orderId},
                success:function (response) {
                    if (response){
                        $("#driver_id").html(response);
                    }
                    else {
                        alert('Something went wrong. Please try again later.');
                    }
                }
            });
            $('#myModal').modal("show");
        }

        // $('button[name="remove_slider_group"]').on('click', function(e) {
        //     var $form = $(this).closest('form');
        //     e.preventDefault();
        //     $('#confirm').modal({
        //         backdrop: 'static',
        //         keyboard: false
        //     })
        //         .one('click', '#delete', function(e) {
        //             $form.trigger('submit');
        //         });
        // });

    </script>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
