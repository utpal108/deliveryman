<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Slider;
use App\SliderGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders=Slider::with('slider_group')->get();
        return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slider_groups=SliderGroup::all();
        return view('admin.slider.create', compact('slider_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'slider_group_id' => 'exists:slider_groups,id',
            'title' => 'required'
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        Slider::create($allData);
        flash('Slider created successfully')->success();
        return redirect()->action('Admin\SliderController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider_groups=SliderGroup::all();
        $slider=Slider::find($id);
        return view('admin.slider.edit',compact('slider','slider_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'slider_group_id' => 'exists:slider_groups,id',
            'title' => 'required',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $slider=Slider::find($id);
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        $slider->details=$request->details;
        $slider->details_url=$request->details_url;
        $slider->slider_group_id=$request->slider_group_id;
        $slider->save();
        flash('Slider updated successfully')->success();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::find($id);
        Storage::delete($slider->slider_image);
        Slider::destroy($id);
        flash('Slider deleted successfully')->success();
        return redirect()->action('Admin\SliderController@index');
    }
}
