<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::where([['user_type','admin'],['status',1]])->get();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Role::all();
        return view('admin.user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:6',
            'phone_no' => 'required',
            'address' => 'required',
            'status' => 'required|integer|min:0|max:1',
        ]);

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);
        $allData['user_type']='admin';

        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }
        $user=User::create($allData);
        $user->assignRole($request->roles);
        flash('Administrator created successfully');
        return redirect()->action('Admin\UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles=Role::all();
        $user=User::find($id);
        return view('admin.user.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'name'=>'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'phone_no' => 'required',
            'address' => 'required',
            'status' => 'required|integer|min:0|max:1',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->phone_no=$request->phone_no;
        $user->address=$request->address;
        $user->status=$request->status;
        if (isset($request->password)){
            $user->password=bcrypt($request->password);
        }

        if ($request->hasFile('profile_image')){
            Storage::delete($user->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $user->profile_image=$path;
        }
        $user->save();
        $user->assignRole($request->roles);
        flash('Administrator updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash('Administrator deleted successfully');
        return redirect()->action('Admin\UserController@index');
    }

    public function login(){
        return view('admin.user.login');
    }

    public function logout(){
        Auth::logout();
        return redirect()->action('UserController@login');
    }

    public function authenticate(Request $request){
        if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'user_type'=>'admin','status'=>1])){
            return redirect()->action('Admin\DashboardController@index');
        }
        else{
            flash('Invalid username or password')->error();
            return redirect()->back();
        }
    }

}
