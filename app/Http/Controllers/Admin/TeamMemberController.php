<?php

namespace App\Http\Controllers\Admin;

use App\TeamMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TeamMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team_members=TeamMember::orderBy('order')->get();
        return view('admin.team_member.index',compact('team_members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team_member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'designation'=>'required',
        ]);

        $allData=$request->all();
        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(255, 300)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }


        TeamMember::create($allData);
        flash('Team member created successfully');
        return redirect()->action('Admin\TeamMemberController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team_member=TeamMember::findOrFail($id);
        return view('admin.team_member.edit',compact('team_member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'designation'=>'required',
        ]);

        $team_member=TeamMember::findOrFail($id);
        $team_member->name=$request->name;
        $team_member->designation=$request->designation;
        $team_member->details=$request->details;

        if ($request->hasFile('profile_image')){
            Storage::delete($team_member->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(255, 300)->encode();
            Storage::put($path, $image);
            $team_member->profile_image=$path;
        }

        $team_member->save();
        flash('Team member updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team_member=TeamMember::findOrFail($id);
        Storage::delete($team_member->profile_image);
        TeamMember::destroy($id);
        flash('Team member deleted successfully');
        return redirect()->action('Admin\TeamMemberController@index');
    }

    public function order_member(Request $request)
    {
        $memberOrders = json_decode($request->member_orders);
        foreach ($memberOrders as $index => $teamMember) {
            $member = TeamMember::findOrFail($teamMember->id);
            $member->order = $index + 1;
            $member->save();
        }
    }
}
