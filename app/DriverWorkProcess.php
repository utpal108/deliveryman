<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverWorkProcess extends Model
{
    protected $fillable=['title','details','icon'];
}
