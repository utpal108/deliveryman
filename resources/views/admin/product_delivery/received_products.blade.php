@extends('admin.layout.app')

@section('page_title','Admin | Received Products')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\ProductDeliveryController@received_products') }}">Received Products</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard <span>> Received Products</span></h1>
            </div>
        </div>
        <div>
            <div>

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-12">
                        @include('flash::message')
                        <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <div class="widget-header">
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                        <h2>All received products </h2>
                                    </div>
                                    <div class="widget-toolbar">
                                        <!-- add: non-hidden - to disable auto hide -->
                                    </div>
                                </header>
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body p-0">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone" class="text-center">ID</th>
                                                <th data-class="expand" class="text-center"> Customer Name</th>
                                                <th data-class="expand" class="text-center"> Phone No</th>
                                                <th data-class="expand" class="text-center"> Ecommerce Site</th>
                                                <th data-class="expand" class="text-center"> Order No</th>
                                                <th data-class="expand" class="text-center"> Receiving Date</th>
                                                <th data-class="expand" class="text-center"> Received By</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($received_products as $index=>$received_product)
                                                <tr class="text-center">
                                                    <td>{{ $index+1 }}</td>
                                                    <td>{{ $received_product->customer['name'] }}</td>
                                                    <td>{{ $received_product->customer['phone_no']  }}</td>
                                                    <td>{{ $received_product->ecommerce_site }}</td>
                                                    <td>{{ $received_product->order_no }}</td>
                                                    <td>{{ $received_product->receiving_date }}</td>
                                                    <td>{{ $received_product->product_received['name'] }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>
                        <!-- WIDGET END -->

                    </div>

                    <!-- end row -->

                    <!-- end row -->

                </section>
                <!-- end widget grid -->

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
