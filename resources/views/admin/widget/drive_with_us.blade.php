@extends('admin.layout.app')

@section('page_title','Admin | Edit Drive With Us')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\WidgetController@index') }}">Widgets</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit Widget</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" method="post" action="{{ action('Admin\WidgetController@update',$widget->id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Drive With Us</h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Data
                                            </legend>
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="contents[title]" value="{{ $widget->contents['title'] ?? '' }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Subtitle</label>
                                                <input type="text" class="form-control" name="contents[subtitle]" value="{{ $widget->contents['subtitle'] ?? '' }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>URL</label>
                                                <input type="text" class="form-control" name="contents[url]" value="{{ $widget->contents['url'] ?? '' }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Drive with us image (352X480)</label>
                                                <div class="box-body">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                            <img src="@if(isset($widget->contents['drive_with_us_image'])){{ '/storage/' .$widget->contents['drive_with_us_image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="drive with us image">
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                        <div class="text-center">
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                                <input type="file" name="drive_with_us_image" @if(!isset($widget->contents['drive_with_us_image'])){{ 'required' }} @endif>
                                                            </span>
                                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {


            // For Editor

            var editor_config = {
                path_absolute : "/",
                selector: "textarea.editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            tinymce.init(editor_config);

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    group_name : {
                        validators : {
                            notEmpty : {
                                message : 'Group name is required'
                            },
                        }
                    }
                }
            });

            // end profile form

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
