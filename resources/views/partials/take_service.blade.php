<section class="missed-delivers ic-bottom-section">
    <div class="container text-center">
        <div class="text">{{ $service->contents['title'] ?? '' }}</div>
        <h1>{{ $service->contents['subtitle'] ?? '' }}</h1>
        <a href="{{ $service->contents['url'] ?? '' }}" class="ic-btn-primary">Take Services</a>
    </div>
</section>
